﻿using CSharp_OOP.BaccaRiccardo.TrafficLight;
using static CSharp_OOP.BaccaRiccardo.TrafficLight.ITrafficLight.Phases;
using static CSharp_OOP.BaccaRiccardo.TrafficLight.DirOfMovement;
using CSharp_OOP.BattistiniRiccardo.Point;
using CSharp_OOP.BattistiniRiccardo.TrafficLight;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace OOP_Test.BattistiniRiccardo
{
    /// <summary>
    /// Test if traffic lights cycle through phases correctly.
    /// </summary>
    [TestClass]
    public class TrafficLightTest
    {
        private LinkedList<KeyValuePair<ITrafficLight.Phases, int>> list;
        private PhaseManager<ITrafficLight.Phases> phMan;
        private IList<ITrafficLight> trafficLights;

        private readonly ITrafficLight tr1 = TrafficLightImpl.CreateTrafficLight(
            Point.Of(1, 2),
            EAST_WEST,
            GREEN
        );

        private readonly ITrafficLight tr2 = TrafficLightImpl.CreateTrafficLight(
            Point.Of(1, 2),
            EAST_WEST,
            RED
        );

        private readonly ITrafficLight tr3 = TrafficLightImpl.CreateTrafficLight(
            Point.Of(1, 2),
            EAST_WEST,
            GREEN
         );

        /// <summary>
        /// parameters initialization.
        /// </summary>
        [TestInitialize]
        public void Initialize()
        {
            list = new LinkedList<KeyValuePair<ITrafficLight.Phases, int>>();

            list.AddFirst(
                new KeyValuePair<ITrafficLight.Phases, int>(GREEN, 3)
            );

            list.AddFirst(
                new KeyValuePair<ITrafficLight.Phases, int>(YELLOW, 1)
            );

            list.AddFirst(
                new KeyValuePair<ITrafficLight.Phases, int>(RED, 2)
            );

            list.AddFirst(
                new KeyValuePair<ITrafficLight.Phases, int>(YELLOW, 1)
            );

            phMan = new PhaseManager<ITrafficLight.Phases>(list);

            trafficLights = new List<ITrafficLight>
            {
                tr1,
                tr2,
                tr3
            };

            phMan.Attach(tr1);
            phMan.Attach(tr2);
            phMan.Attach(tr3);
        }

        /// <summary>
        /// Test if a single traffic light works as expected.
        /// </summary>
        [TestMethod]
        public void TestSinglePhaseChange()
        {
            for (int i = 0; i < 3; i++)
                phMan.Update();

            Assert.AreEqual(YELLOW, tr1.GetCurrentPhase());

            for (int i = 0; i < 3; i++)
                phMan.Update();

            Assert.AreEqual(RED, tr1.GetCurrentPhase());

            for (int i = 0; i < 18; i++)
                phMan.Update();

            Assert.AreEqual(GREEN, tr1.GetCurrentPhase());

            for (int i = 0; i < 6; i++)
                phMan.Update();

            Assert.AreEqual(RED, tr1.GetCurrentPhase());
        }

        /// <summary>
        /// Test if a group of traffic lights works as expected.
        /// </summary>
        [TestMethod]
        public void TestSyncronizedPhaseChange()
        {
            for (int i = 0; i < 4; i++)
                phMan.Update();

            Assert.AreEqual(YELLOW, tr1.GetCurrentPhase());
            Assert.AreEqual(YELLOW, tr2.GetCurrentPhase());

            for (int i = 0; i < 2; i++)
                phMan.Update();

            Assert.AreEqual(RED, tr1.GetCurrentPhase());
            Assert.AreEqual(GREEN, tr2.GetCurrentPhase());

            for (int i = 0; i < 7; i++)
                phMan.Update();

            Assert.AreEqual(GREEN, tr1.GetCurrentPhase());
            Assert.AreEqual(RED, tr2.GetCurrentPhase());
        }


    }
}
