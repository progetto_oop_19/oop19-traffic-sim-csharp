﻿using CSharp_OOP.BaccaRiccardo.Grid;
using CSharp_OOP.BattistiniRiccardo.Point;
using CSharp_OOP.CamporesiStefano.Cell;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;

namespace  OOP_Test.BaccaRiccardo.GridTest
{
    [TestClass]
    public class GridTest
    {
        [TestMethod]
        public void TestGrid()
        {
            int SIZE = 10;
            IGridFactory<ICell> Factory = new GridFactoryImpl<ICell>();
            IGrid<ICell> grid = Factory.Create(SIZE, SIZE);

            Assert.AreEqual(SIZE, grid.GetNumberColumns());
            Assert.AreEqual(SIZE, grid.GetNumberRows());

            List<int> list = new List<int>();
            for (int i = 0; i < 10; i++)
            {
                //setto tutta la prima riga con i numeri in ordine crescente da 0 a 10
                grid.SetElement(Point.Of(1,i), new CellImpl(Point.Of(1,i), CellConstraints.SIMPLE));
                list.Add(i);
            }

            Assert.AreEqual(grid.GetGenericMap().Values.Count, list.Count);
        }
    }
}
