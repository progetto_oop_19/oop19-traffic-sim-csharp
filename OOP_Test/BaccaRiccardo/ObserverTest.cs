using CSharp_OOP.BaccaRiccardo.Observer.utils;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;

namespace  OOP_Test.BaccaRiccardo.ObserverTest
{
    [TestClass]
    public class ObserverTest
    {
        [TestMethod]
        public void TestMethod()
        {
            int counter = 0;
            RulesDatabase database = new RulesDatabase();
            RulesObserver observer = new RulesObserver();

            database.Register(observer);
            counter++;
            database.ModifyObjectInstance(counter);
            database.NotifyObservers();

            Assert.AreEqual(counter, observer.GetCounter());
            Assert.AreEqual(database.getObservers().Count(), 1);
        }
    }
}
