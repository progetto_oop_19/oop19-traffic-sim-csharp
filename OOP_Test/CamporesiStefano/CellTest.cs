﻿using CSharp_OOP.BattistiniRiccardo.Point;
using CSharp_OOP.CamporesiStefano.Cell;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;

namespace OOP_Test.CamporesiStefano.CellTest
{
    [TestClass]
    public class CellTest
    {
        [TestMethod]
        public void TestCell()
        {
            int SIZE = 10;
            Dictionary<Point, CellConstraints> cells = new Dictionary<Point, CellConstraints>();

            for (int i = 0; i< SIZE; i++)
            {
                for (int j = 0; j<SIZE; j++)
                {
                    ICell cell = new CellImpl(Point.Of(i, j), CellConstraints.SIMPLE);
                    cells.Add(cell.GetPosition(), cell.GetType());
                }
            }

            foreach (KeyValuePair<Point, CellConstraints> entry in cells)
            {
                Assert.AreEqual(CellConstraints.SIMPLE, entry.Value);
                Assert.IsNotNull(entry.Key);
            }

            ICell c1 = new CellImpl(Point.Of(5, 5), CellConstraints.CROSSROAD);
            ICell c2 = new CellImpl(Point.Of(5, 0), CellConstraints.END);
            ICell c3 = new CellImpl(Point.Of(0, 5), CellConstraints.START);
            ICell c4 = new CellImpl(Point.Of(7, 7), CellConstraints.SIMPLE);

            Assert.AreEqual(CellConstraints.CROSSROAD, c1.GetType());
            Assert.AreEqual(CellConstraints.END, c2.GetType());
            Assert.AreEqual(CellConstraints.START, c3.GetType());
            Assert.IsFalse(c3.GetType() == CellConstraints.CROSSROAD);
        }
    }
}
