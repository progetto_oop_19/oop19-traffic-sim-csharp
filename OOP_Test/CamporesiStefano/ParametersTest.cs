﻿using System.Collections.Generic;
using CSharp_OOP.BaccaRiccardo.TrafficLight;
using CSharp_OOP.CamporesiStefano.ParametersHolder;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace  OOP_Test.CamporesiStefano.ParametersTest
{
    [TestClass]
    public class ParametersTest
    {
        [TestMethod]
        public void TestParameters()
        {
            Dictionary<ControllerVariablesNames, double> parameters = new Dictionary<ControllerVariablesNames, double>();
            parameters.Add(ControllerVariablesNames.NORTH, 3);
            parameters.Add(ControllerVariablesNames.SOUTH, 0);
            parameters.Add(ControllerVariablesNames.EAST, 5);
            parameters.Add(ControllerVariablesNames.WEST, 7);
            parameters.Add(ControllerVariablesNames.MAXVEL, 10);
            parameters.Add(ControllerVariablesNames.NLANES, 2);

            ParametersHolder param = new ParametersHolder(parameters);

            Assert.IsFalse(param.GetScenarioData().GetRoads().Contains(new RoadData("SOUTH_NORTH", 2, DirOfMovement.SOUTH_NORTH)));
            Assert.IsFalse(param.GetScenarioData().GetRoads().Contains(new RoadData("WEST_EAST", 2, DirOfMovement.WEST_EAST)));
            Assert.IsTrue(param.GetScenarioData().GetVehicles().Count == 0);
            param.CreateScenario();
            Assert.IsNotNull(param.GetScenarioData());

            Assert.IsNotNull(param.GetScenarioData().GetRoads());
            foreach (RoadData road in param.GetScenarioData().GetRoads())
            {
                Assert.AreEqual(2, road.GetLanesNumber());
            }

            Assert.IsNotNull(param.GetScenarioData().GetVehicles());

            foreach (VehicleGroupData grp in param.GetScenarioData().GetVehicles())
            {
                Assert.AreEqual(10, grp.GetMaxVel());
            }
        }
    }
}
