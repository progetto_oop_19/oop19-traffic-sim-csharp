﻿using CSharp_OOP.BattistiniRiccardo.Point;
using CSharp_OOP.BaccaRiccardo.TrafficLight;
using CSharp_OOP.CamporesiStefano.Vehicle;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;

namespace OOP_Test.CamporesiStefano.VehicleTest
{
    [TestClass]
    public class VehicleTest
    {
        [TestMethod]
        public void TestVehicle()
        {
            VehicleBuilder builder = new VehicleBuilder();
            List<IVehicle> vehicles = new List<IVehicle>();
            IVehicle v1 = builder.SetMaxVel(10)
                                .SetSense(DirOfMovement.EAST_WEST)
                                .SetStatus(Status.ENTERING)
                                .SetDeparture(39, 77)
                                .SetDestination(77, 39)
                                .SetVelocity(3)
                                .SetAreaOfControl(15)
                                .Build();

            IVehicle v2 = builder.SetMaxVel(10)
                                .SetSense(DirOfMovement.NORTH_SOUTH)
                                .SetStatus(Status.ENTERING)
                                .SetDeparture(0, 39)
                                .SetDestination(39, 39)
                                .SetVelocity(2)
                                .SetAreaOfControl(15)
                                .Build();

            IVehicle v3 = builder.SetMaxVel(10)
                                .SetSense(DirOfMovement.WEST_EAST)
                                .SetStatus(Status.ENTERING)
                                .SetDeparture(39, 0)
                                .SetDestination(39, 77)
                                .SetVelocity(5)
                                .SetAreaOfControl(15)
                                .Build();

            Assert.AreEqual(DirOfMovement.EAST_WEST, v1.GetSense());
            Assert.AreEqual(Status.ENTERING, v2.GetStatus());
            Assert.AreNotEqual(Point.Of(77, 39), v3.GetDestination());

            vehicles.Add(v1);
            vehicles.Add(v2);
            vehicles.Add(v3);

            for (int i = 0; i< 3; i++)
            {
                Assert.IsNotNull(vehicles[i].GetMaxVel());
                Assert.IsNotNull(vehicles[i].GetDeparture());
                Assert.IsNotNull(vehicles[i].GetAreaOfControl());
            }

        }
    }
}
