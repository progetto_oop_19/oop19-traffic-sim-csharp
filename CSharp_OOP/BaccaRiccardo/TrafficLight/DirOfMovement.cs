﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CSharp_OOP.BaccaRiccardo.TrafficLight
{
    public enum DirOfMovement
    {
        /**
         * 
         */
        NORTH_SOUTH,

        /**
         * 
         */
        SOUTH_NORTH,

        /**
         * 
         */
        EAST_WEST,

        /**
         * 
         */
        WEST_EAST
    }
}
