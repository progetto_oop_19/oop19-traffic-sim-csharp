﻿using CSharp_OOP.BattistiniRiccardo.TrafficLight;
using CSharp_OOP.BattistiniRiccardo.Point;

namespace CSharp_OOP.BaccaRiccardo.TrafficLight
{
    public interface ITrafficLight : ITrafficLightObserver
    {
        public enum Phases
        {
            GREEN, RED, YELLOW
        }

        /**
         * @return the status of the traffic light
         */
        Phases GetCurrentPhase();

        /**
         * 
         * @param position where the stop line is located.
         */
        void SetPosition(Point position);

        /**
         * 
         * @return position of the traffichLight
         */
        Point GetPosition();

        /**
         * 
         * @return sense of the lane that it's responsible
         */
        DirOfMovement GetSense();

        /**
         * 
         * @param sense of the lane that it will be responsible
         */
        void SetSense(DirOfMovement sense);
    }
}
