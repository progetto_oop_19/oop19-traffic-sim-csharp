﻿using System.Collections.Generic;
using CSharp_OOP.BattistiniRiccardo.Point;
using static CSharp_OOP.BaccaRiccardo.TrafficLight.ITrafficLight;

namespace CSharp_OOP.BaccaRiccardo.TrafficLight
{
    public class TrafficLightImpl : ITrafficLight
    {
        private Queue<Phases> phases;
        private Point position;
        private DirOfMovement sense;
        private Phases currentPhase;

        public Queue<Phases> GetPhases()
        {
            return this.phases;
        }

        private TrafficLightImpl(Point position, DirOfMovement sense, Phases startingPhase)
        {
            this.phases = new Queue<Phases>();
            this.position = position;
            this.sense = sense;
            this.InitializeQueue(startingPhase);
            this.UpdateQueue();
        }

        /**
         * Static factory to initialize the phases of the trafficLight in the order in
         * which they should succeed according to the constraints of the model.
         *
         * @param position of the traffichLight
         * @param sense of the lane that it's responsible
         * @param startingPhase phase of the start
         * @return a new {@link TrafficLight} which starting phase is Green.
         */
        public static TrafficLightImpl CreateTrafficLight(Point position, DirOfMovement sense, Phases startingPhase)
        {
            return new TrafficLightImpl(position, sense, startingPhase);
        }

        public ITrafficLight.Phases GetCurrentPhase()
        {
            return this.currentPhase;
        }

        private void InitializeQueue(Phases startingPhase)
        {
            Phases other = startingPhase.Equals(Phases.RED) ? Phases.GREEN : Phases.RED;
            this.phases.Enqueue(startingPhase);
            this.phases.Enqueue(Phases.YELLOW);
            this.phases.Enqueue(other);
            this.phases.Enqueue(Phases.YELLOW);
        }

        private void UpdateQueue()
        {
            this.currentPhase = this.phases.Dequeue();
            this.phases.Enqueue(this.currentPhase);
        }

        /**
         * switch the status of the traffic light following the sequence
         * GREEN-YELLOW-RED.
         */
        public void UpdatePhase()
        {
            this.UpdateQueue();
        }

        public Point GetPosition()
        {
            return this.position;
        }

        public DirOfMovement GetSense()
        {
            return this.sense;
        }

        public void SetPosition(Point position)
        {
            this.position = position;
        }

        public void SetSense(DirOfMovement sense)
        {
            this.sense = sense;
        }
    }
}
