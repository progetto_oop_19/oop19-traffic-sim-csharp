﻿using System;
namespace CSharp_OOP.BaccaRiccardo.Observer
{
    public interface IObserver
    {
        /// To take updates from the Subject.
		/// param object"> the object to be returned
		void Update(object @object);
    }
}
