﻿using System;
namespace CSharp_OOP.BaccaRiccardo.Observer
{
    public interface ISubject
    {
        /// To register new observer to the specified Subject.
        /// Param Obj the new observer to be registered
        void Register(IObserver obj);

        /// To unregister an observer from this Subject.
        void Unregister(IObserver obj);

        /// To notify observers for a change has occurred.
        void NotifyObservers();
        void ModifyObjectInstance(object obj);
    }
}
