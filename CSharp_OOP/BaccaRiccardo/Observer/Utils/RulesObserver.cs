﻿

namespace CSharp_OOP.BaccaRiccardo.Observer.utils
{
    public class RulesObserver : IObserver
    {
        private int counter;

        public RulesObserver()
        {
            counter = 0;
        }

        public void Update(object @object)
        {
            this.counter = (int)@object;
        }

        public int GetCounter()
        {
            return counter;
        }
    }
}
