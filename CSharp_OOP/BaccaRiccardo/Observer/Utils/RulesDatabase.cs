﻿using System.Collections.Generic;

namespace CSharp_OOP.BaccaRiccardo.Observer.utils
{

    public class RulesDatabase : ISubject
    {

        private readonly List<IObserver> observers;
        private object subject;

        public RulesDatabase()
        {
            this.observers = new List<IObserver>();
        }




        /// To update current object instance.
        /// param Object the object instance to be passed to observers
        public void ModifyObjectInstance(object obj)
        {
            subject = obj;
            NotifyObservers();
        }

        public void Register(IObserver obj)
        {
            observers.Add(obj);
        }


		public void Unregister(IObserver obj)
		{
            observers.Remove(obj);
        }

        public void NotifyObservers()
        {
            foreach(IObserver obj in observers)
            {
                obj.Update(subject);
            }
        }

        public List<IObserver> getObservers()
        {
            return this.observers;
        }
    }
}