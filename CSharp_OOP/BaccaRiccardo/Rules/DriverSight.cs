﻿using System;
using System.Collections.Generic;
using CSharp_OOP.BaccaRiccardo.TrafficLight;
using CSharp_OOP.BattistiniRiccardo.Point;
using CSharp_OOP.CamporesiStefano.Vehicle;
using static CSharp_OOP.BaccaRiccardo.TrafficLight.DirOfMovement;
using static CSharp_OOP.BaccaRiccardo.TrafficLight.ITrafficLight;

namespace CSharp_OOP.BaccaRiccardo.Rules
{
    public class DriverSight
    {

        public Nullable<int> GetObstacle(IVehicle vehicle, HashSet<IVehicle> set)
        {
            ISet<IVehicle> equalSense = new HashSet<IVehicle>();
            foreach (IVehicle v in set)
            {
                if (v.GetSense().Equals(vehicle.GetSense()) && v.GetPosition().Equals(vehicle.GetPosition()))
                {
                    equalSense.Add(v);
                }
            }


            Point nearestVehiclePos = FindNear(equalSense, vehicle.GetSense(), vehicle.GetPosition());
            if (!nearestVehiclePos.Equals(Point.Of(2000,2000)))
            {
                Console.WriteLine("ubsw");
                switch (vehicle.GetSense())
                {
                    case NORTH_SOUTH:
                        return this.GetDistFromObstacle(vehicle.GetPosition().Y, nearestVehiclePos.Y);
                    case SOUTH_NORTH:
                        return this.GetDistFromObstacle(nearestVehiclePos.Y, vehicle.GetPosition().Y);
                    case EAST_WEST:
                        return this.GetDistFromObstacle(vehicle.GetPosition().X, nearestVehiclePos.X);
                    case WEST_EAST:
                        return this.GetDistFromObstacle(nearestVehiclePos.X, vehicle.GetPosition().X);
                    default:
                        return null;
                }
            }
            return 0;
        }


        /**
         * 
         * @param vehicles
         * @param mySense
         * @param actualPos
         * @return vehicle's distance from the nearest vehicle in front.
         */
        private Point FindNear(ISet<IVehicle> vehicles, DirOfMovement mySense, Point actualPos)
        {
            Point pos = Point.Of(2000, 2000);
            switch (mySense)
            {
                case NORTH_SOUTH:
                    foreach(IVehicle v in vehicles)
                    {
                        if((v.GetPosition().Y < actualPos.Y && v.GetPosition().Y > actualPos.Y - v.GetAreaOfControl()))
                        {
                            if(pos == Point.Of(2000,2000))
                            {
                                pos = v.GetPosition();
                            } else
                            {
                                if(v.GetPosition().Y > pos.Y)
                                {
                                    pos = v.GetPosition();
                                }
                            }
                        }
                    }
                    return pos;

                case SOUTH_NORTH:
                    foreach (IVehicle v in vehicles)
                    {
                        if ((v.GetPosition().Y > actualPos.Y && v.GetPosition().Y < actualPos.Y + v.GetAreaOfControl()))
                        {
                            if (pos == Point.Of(2000,2000))
                            {
                                pos = v.GetPosition();
                            }
                            else
                            {
                                if (v.GetPosition().Y < pos.Y)
                                {
                                    pos = v.GetPosition();
                                }
                            }
                        }
                    }
                    return pos;

                case WEST_EAST:
                    
                    foreach (IVehicle v in vehicles)
                    {
                        if ((v.GetPosition().X > actualPos.X && v.GetPosition().X < actualPos.X + v.GetAreaOfControl()))
                        {
                            if (pos == Point.Of(2000,2000))
                            {
                                pos = v.GetPosition();
                                
                            }
                            else
                            {
                                if (v.GetPosition().X < pos.X)
                                {
                                    pos = v.GetPosition();
                                }
                            }
                        }
                    }
                    return pos;

                case EAST_WEST:
                    foreach (IVehicle v in vehicles)
                    {
                        if ((v.GetPosition().X < actualPos.X && v.GetPosition().X > actualPos.X - v.GetAreaOfControl()))
                        {
                            if (pos == Point.Of(2000,2000))
                            {
                                pos = v.GetPosition();
                            }
                            else
                            {
                                if (v.GetPosition().X > pos.X)
                                {
                                    pos = v.GetPosition();
                                }
                            }
                        }
                    }
                    return pos;

                default:
                    return pos;
            }
        }

        /**
         * 
         * @param pos1
         * @param pos2
         * @return distance from 2 integers.
         */
        private Nullable<int> GetDistFromObstacle(int pos1, int pos2)
        {
            return (pos1 - pos2 - 1);
        }


        /**
         * 
         * @param vehicle the selected vehicle
         * @param trafficLights all the traffic lights of the simulation
         * @return map composed by traffic light phase and distance, based on what the vehicle can see.
         */
        public IDictionary<Phases, Nullable<int>> GetTrafficLightObstacle(IVehicle vehicle, ISet<ITrafficLight> trafficLights)
        {
            ITrafficLight light = null;
            foreach (ITrafficLight light2 in trafficLights)
            {
                if (light2.GetSense().Equals(vehicle.GetSense()))
                {
                    light = light2;
                    break;
                }
            }

            switch (vehicle.GetSense())
            {
                case EAST_WEST:
                    IDictionary<Phases, Nullable<int>> map = new Dictionary<Phases, Nullable<int>>();
                    if ((light.GetPosition().X < vehicle.GetPosition().X && light.GetPosition().X > vehicle.GetPosition().X - vehicle.GetAreaOfControl()))
                    {
                        map.Add(light.GetCurrentPhase(), this.GetDistFromObstacle(vehicle.GetPosition().X, light.GetPosition().X));
                    } else
                    {
                        map.Add(light.GetCurrentPhase(), null);
                    }
                    return map;

                case WEST_EAST:
                    IDictionary<Phases, Nullable<int>> map2 = new Dictionary<Phases, Nullable<int>>();
                    if ((light.GetPosition().X > vehicle.GetPosition().X && light.GetPosition().X < vehicle.GetPosition().X + vehicle.GetAreaOfControl()))
                    {
                        map2.Add(light.GetCurrentPhase(), this.GetDistFromObstacle(light.GetPosition().X, vehicle.GetPosition().X));
                    } else
                    {
                        map2.Add(light.GetCurrentPhase(), null);
                    }
                    return map2;


                case NORTH_SOUTH:
                    IDictionary<Phases, Nullable<int>> map3 = new Dictionary<Phases, Nullable<int>>();
                    if (light.GetPosition().Y < vehicle.GetPosition().Y && (light.GetPosition().Y > vehicle.GetPosition().Y - vehicle.GetAreaOfControl()))
                    {
                        map3.Add(light.GetCurrentPhase(), this.GetDistFromObstacle(vehicle.GetPosition().Y, light.GetPosition().Y));
                    } else
                    {
                        map3.Add(light.GetCurrentPhase(), null);
                    }
                    return map3;

                case SOUTH_NORTH:
                    IDictionary<Phases, Nullable<int>> map4 = new Dictionary<Phases, Nullable<int>>();
                    if ((light.GetPosition().Y > vehicle.GetPosition().Y && light.GetPosition().Y < vehicle.GetPosition().Y + vehicle.GetAreaOfControl()))
                    {
                        map4.Add(light.GetCurrentPhase(), this.GetDistFromObstacle(light.GetPosition().Y, vehicle.GetPosition().Y));
                    }
                    else
                    {
                        map4.Add(light.GetCurrentPhase(), null);
                    }
                    return map4;
                default:
                    return new Dictionary<Phases, Nullable<int>>();
            }
        }
    }
}
