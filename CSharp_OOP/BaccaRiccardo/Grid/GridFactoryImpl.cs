using CSharp_OOP.CamporesiStefano.Cell;

namespace CSharp_OOP.BaccaRiccardo.Grid
{
    public class GridFactoryImpl<T> : IGridFactory<T>
    {

        public IGrid<T> Create(int rows, int columns)
        {
            return new GridImpl<T>(rows, columns);
        }

    }
}
