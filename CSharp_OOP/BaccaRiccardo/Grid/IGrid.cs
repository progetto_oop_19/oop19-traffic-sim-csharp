using System.Collections.Generic;
using CSharp_OOP.BattistiniRiccardo.Point;
using CSharp_OOP.CamporesiStefano.Cell;

/**
* Interface defining a grid.
*
* @param <T> the type of elements of the grid
*/
namespace CSharp_OOP.BaccaRiccardo.Grid
{
    public interface IGrid<T>
    {

        /**
         * @return the number of rows
         */
        int GetNumberRows();

        /**
         * @return the number of columns
         */
        int GetNumberColumns();

        /**
         * @return a new stream composed by the keys of the genericMap
         */
        IEnumerator<IPoint> GetKeyStream();

        /**
         * @return a stream composed by the values of the genericMap
         */
        IEnumerator<T> GetValueStream();

        /**
         * 
         * @param pos the pos of where the element is
         * @return the value associated with this position
         */
        T GetElement(IPoint pos);

        /**
         * 
         * @param genericMap the new map ready to be set
         */
        void SetGenericMap(Dictionary<IPoint, T> genericMap);

        /**
         *
         * @return the map
         */
        Dictionary<IPoint, T> GetGenericMap();

        /**
         * 
         * @param pos the key of the map
         * @param value the value of the map to be set
         */
        void SetElement(IPoint pos, T value);

    }
}
