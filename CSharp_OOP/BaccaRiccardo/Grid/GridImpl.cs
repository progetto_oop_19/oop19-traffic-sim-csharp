using System.Collections.Generic;
using CSharp_OOP.BattistiniRiccardo.Point;
using CSharp_OOP.CamporesiStefano.Cell;

/**
* Concrete implementation of a grid.
* 
* @param <T> the type of elements of the grid
*/
namespace CSharp_OOP.BaccaRiccardo.Grid
{

    public class GridImpl<T> : IGrid<T>
    {

        private Dictionary<IPoint, T> genericMap;

        private readonly int rows;
        private readonly int columns;

        /**
         *
         * @param rows the number of rows
         * @param columns the number of columns
         */
        public GridImpl(int rows, int columns)
        {
            this.genericMap = new Dictionary<IPoint, T>();
            this.rows = rows;
            this.columns = columns;
        }

        /**
         * @return number of rows in this grid
         */
        public int GetNumberRows() {
            return this.rows;
        }

        /**
         * @return number of columns in this grid
         */
        public int GetNumberColumns() {
            return this.columns;
        }

        /**
         * @return a stream of genericMap keySet
         */
        public IEnumerator<IPoint> GetKeyStream() {
            return this.genericMap.Keys.GetEnumerator();
        }

        /**
         * @return a generic stream for genericMap values
         */
        public IEnumerator<T> GetValueStream() {
            return this.genericMap.Values.GetEnumerator();
        }

        /**
         * @return the element in the genericMap at the specified position
         * @param pos the key of the associated value to be returned
         */
        public T GetElement(IPoint pos)
        {
            return genericMap[pos];
        }

        /**
         * @return the genericMap
         */
        public Dictionary<IPoint, T> GetGenericMap() {
            return this.genericMap;
        }

        /**
         * @param genericMap the genericMap to set
         */
        public void SetGenericMap(Dictionary<IPoint, T> genericMap) {
            this.genericMap = genericMap;
        }


        public void SetElement(IPoint pos, T value) {
            genericMap.Add(pos, value);
        }
    }
}
