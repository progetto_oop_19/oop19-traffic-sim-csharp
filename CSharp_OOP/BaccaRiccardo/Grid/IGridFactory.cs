
using CSharp_OOP.CamporesiStefano.Cell;

namespace CSharp_OOP.BaccaRiccardo.Grid {

    public interface IGridFactory<T>
    {
        /**
         *
         * @param <T>
         * @param rows the number of rows
         * @param columns the number of columns
         * @return new empty grid.
         */
        IGrid<T> Create(int rows, int columns);

    }
}
