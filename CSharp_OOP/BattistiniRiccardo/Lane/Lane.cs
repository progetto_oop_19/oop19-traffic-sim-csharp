﻿using System;
using QuickGraph;
using CSharp_OOP.BaccaRiccardo.TrafficLight;
using static CSharp_OOP.BaccaRiccardo.TrafficLight.DirOfMovement;
using CSharp_OOP.BattistiniRiccardo.Point;
using static CSharp_OOP.CamporesiStefano.Cell.CellConstraints;
using CSharp_OOP.CamporesiStefano.Cell;
using CSharp_OOP.BaccaRiccardo.Grid;
using static System.Math;

namespace CSharp_OOP.BattistiniRiccardo.Lane
{

    /// <summary>
    /// this class employs a fluent interface, it means every method returns an
    /// instance of the class itself that you are trying to modify.
    /// 
    /// </summary>
    public class Lane : ILane
    {
        private readonly IGrid<ICell> grid;
        private readonly int lanesNumber;

        /// <param name="start"> Point of the lane </param>
        /// <param name="end"> Point of the lane </param>
        /// <param name="grid"> of the scenario </param>
        /// <param name="lanesNumber"> number of the lane on the road </param>
        public Lane(IPoint start, IPoint end, IGrid<ICell> grid, int lanesNumber)
        {
            this.Start = start;
            this.End = end;
            this.lanesNumber = lanesNumber;
            this.grid = grid;
            LaneGraph = new AdjacencyGraph<ICell, IEdge<ICell>>();
            FindSense();
            CreateLane();
        }

        /// <returns> the sense of the lane. </returns>
        private void FindSense()
        {

            if (Start.X == End.X)
            {
                Sense = Start.Y < End.Y ? SOUTH_NORTH : NORTH_SOUTH;
            }
            if (Start.Y == End.Y)
            {
                Sense = Start.X < End.X ? WEST_EAST : EAST_WEST;
            }

        }

        /// <returns> a lane where cell start type and end type are set. </returns>
        private Lane SetStartEnd()
        {
            grid.GetElement(Start).SetType(START);
            grid.GetElement(End).SetType(END);
            return this;
        }

        /// <returns> the cell that need to be set. </returns>
        private Lane SetCrossroad()
        {

            if (Sense.Equals(EAST_WEST) || Sense.Equals(WEST_EAST))
            {
                switch (lanesNumber)
                {

                    case 1:
                        grid.GetElement(Point.Point.Of((int)Ceiling((double)GridConstraints.GRID_HEIGHT / (2)), Start.Y)).SetType(CROSSROAD);
                        break;

                    case 2:
                        grid.GetElement(Point.Point.Of((int)Ceiling((double)GridConstraints.GRID_HEIGHT / 2), Start.Y)).SetType(CROSSROAD);
                        grid.GetElement(Point.Point.Of((int)Ceiling((double)GridConstraints.GRID_HEIGHT / (2)) + 1, Start.Y)).SetType(CROSSROAD);
                        break;
                }
            }
            else
            {
                switch (lanesNumber)
                {
                    case 1:
                        grid.GetElement(Point.Point.Of(Start.X, (int)Ceiling((double)GridConstraints.GRID_WIDTH / (2)))).SetType(CROSSROAD);
                        break;
                    case 2:
                        grid.GetElement(Point.Point.Of(Start.X, (int)Ceiling((double)GridConstraints.GRID_WIDTH / (2)))).SetType(CROSSROAD);
                        grid.GetElement(Point.Point.Of(Start.X, (int)Ceiling((double)GridConstraints.GRID_WIDTH / (2)) + 1)).SetType(CROSSROAD);
                        break;
                }
            }
            return this;
        }

        /// <returns> the parameters needed to create the graph. </returns>
        private IPoint SetParameters()
        {

            int x;
            int y;

            if (Sense.Equals(EAST_WEST) || Sense.Equals(WEST_EAST))
            {
                x = Start.X;
                y = End.X;
            }
            else
            {
                x = Start.Y;
                y = End.Y;
            }
            return Point.Point.Of(x, y);
        }

        /// <summary>
        /// create vertices of the graph.
        /// </summary>
        private void CreateVertices()
        {
            IPoint p = SetParameters();

            for (int i = p.X; i <= p.Y; i++)
            {
                if (Sense.Equals(WEST_EAST))
                    LaneGraph.AddVertex(grid.GetElement(Point.Point.Of(i, End.Y)));

                if (Sense.Equals(SOUTH_NORTH))
                    LaneGraph.AddVertex(grid.GetElement(Point.Point.Of(End.X, i)));
            }

            for (int i = p.X; i >= p.Y; i--)
            {
                if (Sense.Equals(EAST_WEST))
                    LaneGraph.AddVertex(grid.GetElement(Point.Point.Of(i, End.Y)));

                if (Sense.Equals(NORTH_SOUTH))
                    LaneGraph.AddVertex(grid.GetElement(Point.Point.Of(End.X, i)));
            }
        }

        /// <summary>
        /// create edges of the graph.
        /// </summary>
        private void CreateEdges()
        {
            IPoint p = SetParameters();

            for (int i = p.X; i < p.Y; i++)
            {
                if (Sense.Equals(WEST_EAST))
                {
                    ICell source = grid.GetElement(Point.Point.Of(i, End.Y));
                    ICell target = grid.GetElement(Point.Point.Of(i + 1, End.Y));
                    LaneGraph.AddEdge(new Edge<ICell>(source, target));
                }
                if (Sense.Equals(SOUTH_NORTH))
                {
                    ICell source = grid.GetElement(Point.Point.Of(End.X, i));
                    ICell target = grid.GetElement(Point.Point.Of(End.X, i + 1));
                    LaneGraph.AddEdge(new Edge<ICell>(source, target));
                }
            }

            for (int i = p.X; i > p.Y; i--)
            {
                if (Sense.Equals(EAST_WEST))
                {
                    ICell source = grid.GetElement(Point.Point.Of(i, End.Y));
                    ICell target = grid.GetElement(Point.Point.Of(i - 1, End.Y));
                    LaneGraph.AddEdge(new Edge<ICell>(source, target));
                }
                if (Sense.Equals(NORTH_SOUTH))
                {
                    ICell source = grid.GetElement(Point.Point.Of(End.X, i));
                    ICell target = grid.GetElement(Point.Point.Of(End.X, i - 1));
                    LaneGraph.AddEdge(new Edge<ICell>(source, target));
                }
            }
        }

        /// <summary>
        /// create the lane.
        /// </summary>
        private void CreateLane()
        {
            SetCrossroad();
            SetStartEnd();
            CreateVertices();
            CreateEdges();
        }

        public DirOfMovement Sense { get; private set; }

        public AdjacencyGraph<ICell, IEdge<ICell>> LaneGraph { get; }

        public IPoint Start { get; }

        public IPoint End { get; }

    }
}