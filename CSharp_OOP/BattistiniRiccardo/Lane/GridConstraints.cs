﻿namespace CSharp_OOP.BattistiniRiccardo.Lane
{
	public sealed class GridConstraints
	{
    
		public const int GRID_HEIGHT = 78;

		public const int GRID_WIDTH = 78;

		private GridConstraints() { }
	}

}