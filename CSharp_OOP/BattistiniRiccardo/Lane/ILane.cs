﻿using QuickGraph;
using CSharp_OOP.CamporesiStefano.Cell;
using CSharp_OOP.BaccaRiccardo.TrafficLight;
using CSharp_OOP.BattistiniRiccardo.Point;

namespace CSharp_OOP.BattistiniRiccardo.Lane
{
	public interface ILane
	{

        /// <returns> sense of the lane </returns>
        DirOfMovement Sense { get; }

        /// <returns> Immutable graph relative a lane </returns>
        AdjacencyGraph<ICell, IEdge<ICell>> LaneGraph { get; }

        /// <returns> the start point of the lane </returns>
        IPoint Start { get; }

        /// <returns> the end point of the lane </returns>
        IPoint End { get; }
    }

}