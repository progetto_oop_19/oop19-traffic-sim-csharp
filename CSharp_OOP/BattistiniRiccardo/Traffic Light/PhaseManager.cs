using System.Collections.Generic;

namespace CSharp_OOP.BattistiniRiccardo.TrafficLight
{

    /// <summary>
    /// Concrete implementation of the PhaseManager.
    /// </summary>
    /// <param name="= T"> the object that represents a phase </param>
    public sealed class PhaseManager<T> : IPhaseManager<T>
	{

		private readonly LinkedList<KeyValuePair<T, int>> phaseQueue;
		private readonly ISet<ITrafficLightObserver> observers;
        private KeyValuePair<T, int> currentPhase;

		/// <summary>
		/// Creates a new phaseManager with the given list.
		/// </summary>
		/// <param name="list"> the list of phases to be managed </param>
		public PhaseManager(LinkedList<KeyValuePair<T, int>> list)
		{
            if (list != null)
                phaseQueue = new LinkedList<KeyValuePair<T, int>>(list);
            observers = new HashSet<ITrafficLightObserver>();
            Initialize();
            CurrentPhaseDuration = currentPhase.Value;
		}

		private void Initialize()
		{
            currentPhase = phaseQueue.Last.Value;
            phaseQueue.RemoveFirst();
            phaseQueue.AddLast(currentPhase);
		}

		/// <summary>
		/// <inheritdoc />
		/// </summary>
		public void Update()
		{
            CurrentPhaseDuration--;

			if (CurrentPhaseDuration == 0)
			{
				Initialize();
				CurrentPhaseDuration = currentPhase.Value;
				NotifyObservers();
			}
        }

        /// <summary>
        /// <inheritdoc />
        /// </summary>
        public void Attach(ITrafficLightObserver obs) => observers.Add(obs);

        /// <summary>
        /// <inheritdoc />
        /// </summary>
        public void Detach(ITrafficLightObserver obs) => observers.Remove(obs);

        /// <summary>
        /// <inheritdoc />
        /// </summary>
        public void NotifyObservers()
		{
			foreach (var observer in observers)
			{
				observer.UpdatePhase();
			}
        }

        /// <summary>
        /// <inheritdoc />
        /// </summary>
        public T CurrentPhase => currentPhase.Key;

        /// <summary>
        /// <inheritdoc />
        /// </summary>
        public int CurrentPhaseDuration { get; private set; }
    }

}