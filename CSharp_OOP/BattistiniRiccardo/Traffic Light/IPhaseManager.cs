﻿namespace CSharp_OOP.BattistiniRiccardo.TrafficLight
{
    /// 
    /// <summary>
    /// Interface that defines an object for managing phase's transition of traffic
    /// lights.
    /// 
    /// This component handles a fixed amount of phases, specified at the moment of
    /// creation. Each phase has a duration identified by an integer. The manager
    /// transitions to each phase always with the same order specified in the list
    /// given.
    /// </summary>
    /// <param name="= T"> the object that represents a phase </param>
    public interface IPhaseManager<T> : ITrafficLightSubject
    {

		/// <summary>
		/// Tells the phaseManager that a slice of time is passed. Eventually the phase
		/// manager will update the phase of each TrafficLight that is currently
		/// observing.
		/// </summary>
		void Update();

		/// <returns> The current phase </returns>
		T CurrentPhase {get;}

		/// <returns> The time remaining until the next phase change </returns>
		int CurrentPhaseDuration {get;}

	}

}