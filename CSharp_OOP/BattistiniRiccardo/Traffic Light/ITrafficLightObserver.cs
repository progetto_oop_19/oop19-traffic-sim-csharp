﻿namespace CSharp_OOP.BattistiniRiccardo.TrafficLight
{
	/// <summary>
	/// Provides an implementation of the observer, used to update traffic lights. 
	/// </summary>
	public interface ITrafficLightObserver
    {

		/// <summary>
		/// Tells the traffic light to update its state.
		/// </summary>
		void UpdatePhase();
	}

}