﻿namespace CSharp_OOP.BattistiniRiccardo.TrafficLight
{
	public interface ITrafficLightSubject
    {

		/// <summary>
		/// To register a new observer to the specified subject.
		/// </summary>
		/// <param name="obs"> the observer to be added </param>
		void Attach(ITrafficLightObserver obs);

		/// <summary>
		/// To unregister an observer from this subject.
		/// </summary>
		/// <param name="obs"> the observer to be removed </param>
		void Detach(ITrafficLightObserver obs);

		/// <summary>
		/// To notify observers that their state should be updated.
		/// </summary>
		void NotifyObservers();

	}

}