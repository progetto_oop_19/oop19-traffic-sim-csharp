﻿namespace CSharp_OOP.BattistiniRiccardo.Point
{
	/// <summary>
	/// An immutable point in a two-dimensional space composed of pairs of integers.
	/// </summary>
	public interface IPoint
    {

        /// <returns> the X coordinate </returns>
        int X { get; }

        /// <summary>
        /// Changes the current abscissa, incrementing or decrementing its value
        /// depending on the argument passed.
        /// </summary>
        /// <param name="x"> the value that will be added to the current abscissa </param>
        /// <returns> a new immutable Point2D with modified abscissa </returns>
        Point TranslateX(int x);

        /// <returns> the Y coordinate </returns>
        int Y { get; }

        /// <summary>
        /// Changes the current ordinate, incrementing or decrementing its value
        /// depending on the argument passed.
        /// </summary>
        /// <param name="y"> the value that will be added to the current ordinate </param>
        /// <returns> a new immutable Point2D with modified ordinate </returns>
        Point TranslateY(int y);

        /// <returns> the point. </returns>
        Point Position { get; }

        /// <summary>
        /// Changes the current coordinate, incrementing or decrementing its value
        /// depending on the arguments passed.
        /// </summary>
        /// <param name="x"> the value that will be added to the current abscissa </param>
        /// <param name="y"> the value that will be added to the current ordinate </param>
        /// <returns> a new immutable Point2D </returns>
        Point TranslatePosition(int x, int y);
	}

}