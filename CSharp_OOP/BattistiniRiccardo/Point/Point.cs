﻿using System;
using System.Collections.Generic;

namespace CSharp_OOP.BattistiniRiccardo.Point
{
    /// <summary>
    /// A point representing a position in (x,y) coordinate space, specified
    /// in integer precision.
    /// </summary>
    public sealed class Point : IPoint
    {
        private Point(int x, int y)
        {
            X = x;
            Y = y;
        }

        /// <summary>
        /// Returns an instance of Point2D.
        /// </summary>
        /// <param name="x"> the X coordinate </param>
        /// <param name="y"> the Y coordinate </param>
        /// <returns> sad </returns>
        public static Point Of(int x, int y)
        {
            return new Point(x, y);
        }

        /// <summary>
        /// <inheritdoc />
        /// </summary>
        public int X { get; }

        /// <summary>
        /// <inheritdoc />
        /// </summary>
        public Point TranslateX(int x) => Of(X + x, Y);

        /// <summary>
        /// <inheritdoc />
        /// </summary>
        public int Y { get; }

        /// <summary>
        /// <inheritdoc />
        /// </summary>
        public Point TranslateY(int y) => Of(X, Y + y);

        /// <summary>
        /// <inheritdoc />
        /// </summary>
        public Point Position => this;

        /// <summary>
        /// <inheritdoc />
        /// </summary>
        public Point TranslatePosition(int x, int y) => Of(X + x, Y + y);

        /*public override bool Equals(object obj) => obj is Point point &&
EqualityComparer<Point>.Default.Equals(Position, point.Position);


        public override int GetHashCode() => HashCode.Combine(Position);*/

        public override string ToString() => string.Format("Point [x={0}, y={1}]", X, Y);


    }

}