﻿using System.Collections.Generic;
using static CSharp_OOP.BaccaRiccardo.TrafficLight.DirOfMovement;
using CSharp_OOP.CamporesiStefano.ParametersHolder;
using CSharp_OOP.BattistiniRiccardo.Point;
using CSharp_OOP.CamporesiStefano.Cell;
using CSharp_OOP.BaccaRiccardo.Grid;

namespace CSharp_OOP.BattistiniRiccardo.Road
{

    public class OneWayRoad : NWayRoad
    {

        private LinkedList<IPoint> startPoint;
        private readonly LinkedList<IPoint> endPoint;

        /// <param name="info"> of the road </param>
        /// <param name="grid"> of the scenario </param>
        public OneWayRoad(RoadData info, IGrid<ICell> grid) : base(grid, info)
        {
            startPoint = new LinkedList<IPoint>();
            endPoint = new LinkedList<IPoint>();
        }

        /// <summary>
        /// <inheritdoc />
        /// </summary>
        internal override LinkedList<IPoint> FindStart(BaccaRiccardo.TrafficLight.DirOfMovement sense)
        {
            switch (sense)
            {

                case WEST_EAST:
                    startPoint.AddLast(Point.Point.Of(0, DistanceRow()));
                    break;

                case EAST_WEST:
                    startPoint.AddLast(Point.Point.Of(grid.GetNumberColumns() - 1, DistanceRow()));
                    break;

                case NORTH_SOUTH:
                    startPoint.AddLast(Point.Point.Of(DistanceCol(), grid.GetNumberRows() - 1));
                    break;

                case SOUTH_NORTH:
                    startPoint.AddLast(Point.Point.Of(DistanceCol(), 0));
                    break;
            }
            return startPoint;
        }

        /// <summary>
        /// <inheritdoc />
        /// </summary>
        internal override LinkedList<IPoint> FindEnd(BaccaRiccardo.TrafficLight.DirOfMovement sense)
        {

            switch (sense)
            {

                case WEST_EAST:
                    endPoint.AddLast(Point.Point.Of(grid.GetNumberColumns() - 1, DistanceRow()));
                    break;

                case EAST_WEST:
                    endPoint.AddLast(Point.Point.Of(0, DistanceRow()));
                    break;

                case NORTH_SOUTH:
                    endPoint.AddLast(Point.Point.Of(DistanceCol(), 0));
                    break;

                case SOUTH_NORTH:
                    endPoint.AddLast(Point.Point.Of(DistanceCol(), grid.GetNumberRows() - 1));
                    break;
            }
            return endPoint;
        }
    }

}