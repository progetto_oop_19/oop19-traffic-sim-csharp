﻿using System.Collections.Generic;
using CSharp_OOP.BattistiniRiccardo.Lane;

namespace CSharp_OOP.BattistiniRiccardo.Road
{

	public interface IRoad
    {
		/// <summary>
		/// create the road.
		/// </summary>
		void CreateRoad();

		/// <returns> the road that was created. </returns>
		IDictionary<string, ISet<ILane>> Roads {get;}
	}

}