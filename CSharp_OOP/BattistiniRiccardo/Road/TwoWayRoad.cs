﻿using System.Collections.Generic;
using static CSharp_OOP.BaccaRiccardo.TrafficLight.DirOfMovement;
using CSharp_OOP.CamporesiStefano.ParametersHolder;
using CSharp_OOP.BattistiniRiccardo.Lane;
using CSharp_OOP.BattistiniRiccardo.Point;
using CSharp_OOP.CamporesiStefano.Cell;
using CSharp_OOP.BaccaRiccardo.Grid;

namespace CSharp_OOP.BattistiniRiccardo.Road
{

	public class TwoWayRoad : NWayRoad
	{

			private readonly LinkedList<IPoint> startPoints;
			private LinkedList<IPoint> endPoints;


		/// <param name="info"> of the road </param>
		/// <param name="grid"> grid of the scenario </param>
		public TwoWayRoad(RoadData info, IGrid<ICell> grid) : base(grid, info)
		{
			startPoints = new LinkedList<IPoint>();
			endPoints = new LinkedList<IPoint>();

		}

		/// <summary>
		/// <inheritdoc />
		/// </summary>
		internal override LinkedList<IPoint> FindStart(BaccaRiccardo.TrafficLight.DirOfMovement sense)
		{

			if (sense.Equals(WEST_EAST) || sense.Equals(EAST_WEST))
			{
				startPoints.AddLast(Point.Point.Of(0, base.DistanceRow()));
				startPoints.AddLast(Point.Point.Of(grid.GetNumberColumns() - 1, base.DistanceRow() + 1));
			}
			else
			{
				startPoints.AddLast(Point.Point.Of(base.DistanceCol(), grid.GetNumberRows() - 1));
				startPoints.AddLast(Point.Point.Of(base.DistanceCol() + 1, 0));

			}
			return startPoints;
		}

		/// <summary>
		/// <inheritdoc />
		/// </summary>
		internal override LinkedList<IPoint> FindEnd(BaccaRiccardo.TrafficLight.DirOfMovement sense)
		{

			if (sense.Equals(EAST_WEST) || sense.Equals(WEST_EAST))
			{
				endPoints.AddLast(Point.Point.Of(grid.GetNumberColumns() - 1, base.DistanceRow()));
			    endPoints.AddLast(Point.Point.Of(0, base.DistanceRow() + 1));
			}
			else
			{
				endPoints.AddLast(Point.Point.Of(base.DistanceCol(), 0));
				endPoints.AddLast(Point.Point.Of(base.DistanceCol() + 1, grid.GetNumberRows() - 1));
			}
			return endPoints;
		}
	}

}