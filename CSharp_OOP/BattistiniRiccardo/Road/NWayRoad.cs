﻿using System.Collections.Generic;
using static CSharp_OOP.BaccaRiccardo.TrafficLight.DirOfMovement;
using CSharp_OOP.CamporesiStefano.ParametersHolder;
using CSharp_OOP.BattistiniRiccardo.Lane;
using CSharp_OOP.BattistiniRiccardo.Point;
using static CSharp_OOP.CamporesiStefano.Cell.CellConstraints;
using CSharp_OOP.CamporesiStefano.Cell;
using CSharp_OOP.BaccaRiccardo.Grid;
using static System.Math;
using System.Linq;

namespace CSharp_OOP.BattistiniRiccardo.Road
{

	public abstract class NWayRoad : IRoad
	{
        protected readonly IGrid<ICell> grid;
        protected readonly RoadData info;
		private LinkedList<IPoint> startPoint;
		private LinkedList<IPoint> endPoint;
		private readonly IDictionary<string, ISet<ILane>> road;

        /// <param name="grid"> of the scenario </param>
        /// <param name="info"> of the road </param>
        protected NWayRoad(IGrid<ICell> grid, RoadData info)
		{
			this.grid = grid;
			this.info = info;
            startPoint = new LinkedList<IPoint>();
            endPoint = new LinkedList<IPoint>();
            road = new Dictionary<string, ISet<ILane>>();
		}

		/// <returns> the central row of the grid </returns>
		public virtual int DistanceRow()
		{
			int dist;
			dist = (int) Ceiling((double) this.grid.GetNumberRows() / (2));
			return dist;
		}

		/// <returns> the central column of the grid </returns>
		public virtual int DistanceCol()
		{
			int dist;
			dist = (int) Ceiling((double)grid.GetNumberColumns() / 2);
            return dist;
		}

		/// <summary>
		/// set all cells that need to create the lane.
		/// </summary>
		/// <param name="startPoints"> list of the start points of the road </param>
		/// <param name="info"> info of the road </param>
		public virtual void SetCell(LinkedList<IPoint> startPoints, RoadData info)
		{
			foreach (Point.Point p in startPoints)
            {
                if (info.GetSense().Equals(EAST_WEST) || info.GetSense().Equals(WEST_EAST))
                    for (int k = 0; k < grid.GetNumberColumns(); k++)
                        this.grid.SetElement(Point.Point.Of(k, p.Y), new CellImpl(Point.Point.Of(k, p.Y), SIMPLE));
                else
                    for (int k = 0; k < grid.GetNumberColumns(); k++)
                        this.grid.SetElement(Point.Point.Of(p.X, k), new CellImpl(Point.Point.Of(p.X, k), SIMPLE));
            }
        }

		/// <summary>
		/// Method to create a road with 1 lane.
		/// </summary>
		public virtual void CreateRoad()
		{
			ISet<ILane> lanes = new HashSet<ILane>();
			startPoint = FindStart(info.GetSense());
			endPoint = FindEnd(info.GetSense());
			this.SetCell(startPoint, info);
			for (int l = 0; l < info.GetLanesNumber(); l++)
			{
				lanes.Add(new Lane.Lane(startPoint.ElementAt(1), endPoint.ElementAt(1), grid, info.GetLanesNumber()));
			}
			road[info.GetName()] = lanes;


        }

        /// <summary>
        /// <inheritdoc />
        /// </summary>
        public virtual IDictionary<string, ISet<ILane>> Roads => road;

        /// <param name="sense"> of the road </param>
        /// <returns> list of the start points of the road </returns>
        internal abstract LinkedList<IPoint> FindStart(BaccaRiccardo.TrafficLight.DirOfMovement sense);

		/// <param name="sense"> of the road </param>
		/// <returns> list of the end points of the road </returns>
		internal abstract LinkedList<IPoint> FindEnd(BaccaRiccardo.TrafficLight.DirOfMovement sense);
	}

}