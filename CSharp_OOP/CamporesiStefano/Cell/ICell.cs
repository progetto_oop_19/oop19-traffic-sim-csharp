﻿
using CSharp_OOP.BattistiniRiccardo.Point;

namespace CSharp_OOP.CamporesiStefano.Cell
{
    public interface ICell
    {
        /**
         * 
         * @return the position of the cell
         */
        Point GetPosition();

        /**
         * 
         * @param point position of the cell
         */
        void SetPosition(Point point);


        /**
          * 
          * @return the type of the cell
          */
        CellConstraints GetType();

        /**
          * 
          * @param type set the type of the cell
          */
        void SetType(CellConstraints type);
    }
}
