﻿using CSharp_OOP.BattistiniRiccardo.Point;

namespace CSharp_OOP.CamporesiStefano.Cell
{
    public class CellImpl : ICell
    {
        private Point position;
        private CellConstraints typecell;

        public CellImpl(Point position, CellConstraints typecell)
        {
            this.position = position;
            this.typecell = typecell;
        }

        public Point GetPosition()
        {
            return this.position;
        }

        public void SetPosition(Point point)
        {
            this.position = point;
        }

        public new CellConstraints GetType()
        {
            return this.typecell;
        }

        public void SetType(CellConstraints type)
        {
            this.typecell = type;
        }

    }


}

