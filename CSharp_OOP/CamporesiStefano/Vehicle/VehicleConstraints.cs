﻿namespace CSharp_OOP.CamporesiStefano.Vehicle
{

    public enum Status
    {
        /**
         * 
         */
        RUNNING,

        /**
         * 
         */
        IDLE,

        /**
         * 
         */
        ENTERING,

        /**
         * 
         */
        EXITING
    }
}
