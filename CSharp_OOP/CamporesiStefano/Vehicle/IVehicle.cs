﻿using CSharp_OOP.BaccaRiccardo.TrafficLight;
using CSharp_OOP.BattistiniRiccardo.Point;
using System;

namespace CSharp_OOP.CamporesiStefano.Vehicle
{

    public interface IVehicle
    {

        /**
         * @return the vehicle velocity. 
         */
        int GetVelocity();

        /**
         * 
         * @return the sight of the driver
         */
        int GetAreaOfControl();

        /**
         * 
         * @param pos new position of the vehicle
         */
        void SetPosition(Point pos);
        /**
         * 
         * @return the position of the vehicle
         */
        Point GetPosition();

        /**
         * 
         * @return the start position of the vehicle
         */
        Point GetDeparture();

        /**
         * 
         * @return the ending position of the vehicle
         */
        Point GetDestination();

        /**
         * 
         * @param velocity increase the vehicle velocity.
         */
        void SetVelocity(int velocity);

       /**
        * 
        * @return vehicle sense
        */
        DirOfMovement GetSense();

       /**
         * 
         * @return vehicle status
         * 
         */
        Status GetStatus();

        /**
         * 
         * @param status of the vehicle 
         */
        void SetStatus(Status status);


        String ToString();

        /**
         * 
         * @return vehicle maxVel.
         */
        int GetMaxVel();

        /**
         * 
         * @param maxVel max velocity that a vehicle can reach
         */
        void SetMaxVel(int maxVel);
    }
}
