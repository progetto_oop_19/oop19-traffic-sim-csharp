﻿using CSharp_OOP.BaccaRiccardo.TrafficLight;
using CSharp_OOP.BattistiniRiccardo.Point;

namespace CSharp_OOP.CamporesiStefano.Vehicle
{
    public class VehicleBuilder
    {
        private int velocity;
        private Point departure;
        private Point destination;
        private int areaOfControl;
        private DirOfMovement sense;
        private Status status;
        private int maxVel;

        public VehicleBuilder SetMaxVel(int maxVel)
        {
            this.maxVel = maxVel;
            return this;
        }

        public VehicleBuilder SetSense(DirOfMovement sense)
        {
            this.sense = sense;
            return this;
        }

        public VehicleBuilder SetStatus(Status status)
        {
            this.status = status;
            return this;
        }

        public VehicleBuilder SetDeparture( int x, int y)
        {
            this.departure = Point.Of(x,y);
            return this;
        }

        public VehicleBuilder SetDestination(int x,int y)
        {
            this.destination = Point.Of(x,y);
            return this;
        }

        public VehicleBuilder SetVelocity(int vel)
        {
            this.velocity = vel;
            return this;
        }

        public VehicleBuilder SetAreaOfControl(int areaOfC)
        {
            this.areaOfControl = areaOfC;
            return this;
        }

        public IVehicle Build()
        {
            return new VehicleImpl(this.velocity, this.areaOfControl, this.departure, this.destination, this.sense,
                    this.status, this.maxVel);
        }

    }
}
