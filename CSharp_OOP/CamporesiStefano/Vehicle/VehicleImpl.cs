﻿using CSharp_OOP.BaccaRiccardo.TrafficLight;
using System;
using CSharp_OOP.BattistiniRiccardo.Point;

namespace CSharp_OOP.CamporesiStefano.Vehicle
{
    public class VehicleImpl : IVehicle
    {
        private int velocity;
        private readonly int areaOfControll;
        private Point position;
        private readonly Point departure;
        private readonly Point destination;
        private DirOfMovement sense;
        private Status status;
        private int maxVel;

        public VehicleImpl(int vel, int areaOfC, Point start, Point end, DirOfMovement sense, Status status, int maxVel)
        {
            this.velocity = vel;
            this.areaOfControll = areaOfC;
            this.departure = start;
            this.position = start;
            this.destination = end;
            this.sense = sense;
            this.status = status;
            this.maxVel = maxVel;
        }

     
        public int GetVelocity()
            {

                return this.velocity;
            }

      
        public int GetAreaOfControl()
            {
                return this.areaOfControll;
            }

      
        public Point GetPosition()
            {
                return this.position;
            }

       
        public void SetPosition(Point pos)
            {
                this.position = pos;
            }

        
       
        public Point GetDeparture()
            {
                return this.departure;
            }

        
       
        public Point GetDestination()
            {
                return this.destination;
            }

        
        
        public void SetVelocity( int velocity)
            {
                this.velocity = velocity;
            }

        
       
        public DirOfMovement GetSense()
            {
                return this.sense;
            }

     
        public Status GetStatus()
            {
                return this.status;
            }

       
        public void SetStatus(Status status)
            {
                this.status = status;
            }

        override
        public String ToString()
            {
                return "vel = " + this.GetVelocity() + "     pos = " + this.GetPosition() + "    senso = " + this.GetSense();

            }

        public int GetMaxVel()
            {
                return maxVel;
            }

        public void SetMaxVel(int maxVel)
            {
                this.maxVel = maxVel;
            }

        }

}

