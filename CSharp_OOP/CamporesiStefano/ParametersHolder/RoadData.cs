﻿using System;
using CSharp_OOP.BaccaRiccardo.TrafficLight;

namespace CSharp_OOP.CamporesiStefano.ParametersHolder
{
    public class RoadData
    {
        private String name;
        private int lanesNumber;
        private DirOfMovement sense;

        public RoadData(String name, int lanesNumber, DirOfMovement sense)
        {
            this.name = name;
            this.lanesNumber = lanesNumber;
            this.sense = sense;
        }

        public String GetName()
        {
            return name;
        }

        public int GetLanesNumber()
        {
            return lanesNumber;
        }

        public DirOfMovement GetSense()
        {
            return this.sense;
        }

        public void SetName(String name)
        {
            this.name = name;
        }

        public void SetLanesNumber(int lanesNumber)
        {
            this.lanesNumber = lanesNumber;
        }

        public void SetSense(DirOfMovement sense)
        {
            this.sense = sense;
        }
    }
}
