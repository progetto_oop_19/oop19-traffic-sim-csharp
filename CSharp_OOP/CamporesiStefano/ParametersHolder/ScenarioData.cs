﻿using System.Collections.Generic;

namespace CSharp_OOP.CamporesiStefano.ParametersHolder
{
    public class ScenarioData
    {
        private HashSet<VehicleGroupData> vehicles;
        private HashSet<RoadData> roads;
        
        public ScenarioData()
        {
            this.vehicles = new HashSet<VehicleGroupData>();
            this.roads = new HashSet<RoadData>();
        }

        public HashSet<VehicleGroupData> GetVehicles()
        {
            return vehicles;
        }

        public HashSet<RoadData> GetRoads()
        {
            return roads;
        }

        public void SetVehicles(HashSet<VehicleGroupData> vehicles)
        {
            this.vehicles = vehicles;
        }

        public void SetRoads(HashSet<RoadData> roads)
        {
            this.roads = roads;
        }

    }
}
