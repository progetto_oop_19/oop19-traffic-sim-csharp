﻿using System;
using System.Collections.Generic;
using CSharp_OOP.BaccaRiccardo.TrafficLight;

namespace CSharp_OOP.CamporesiStefano.ParametersHolder
{
    public class ParametersHolder
    {
        private readonly IDictionary<ControllerVariablesNames, Double> parameters;
        private ScenarioData scenario;

        public ParametersHolder(Dictionary<ControllerVariablesNames, Double> parameters)
        {
            this.parameters = parameters;
            scenario = new ScenarioData();
        }

        /**
         * create the date of the scenario.
         */
        public void CreateScenario()
        {
            HashSet<VehicleGroupData> vehicleGroup = new HashSet<VehicleGroupData>();
            HashSet<RoadData> roadsData = new HashSet<RoadData>();

            int maxVel = 0;
            foreach(KeyValuePair<ControllerVariablesNames, Double> v in parameters)
            {
                if (v.Key.Equals(ControllerVariablesNames.MAXVEL))
                {
                    maxVel = (int)v.Value;
                }
            }

            foreach (KeyValuePair< ControllerVariablesNames, Double > entry in parameters)
            {

                switch (entry.Key)
                {

                    case  ControllerVariablesNames.NORTH:
                        vehicleGroup.Add(new VehicleGroupData(DirOfMovement.NORTH_SOUTH, (int)entry.Value, maxVel));
                        break;

                    case  ControllerVariablesNames.SOUTH:
                        vehicleGroup.Add(new VehicleGroupData(DirOfMovement.SOUTH_NORTH, (int)entry.Value, maxVel));
                        break;

                    case ControllerVariablesNames.EAST:
                        vehicleGroup.Add(new VehicleGroupData(DirOfMovement.EAST_WEST, (int)entry.Value, maxVel));
                        break;

                    case ControllerVariablesNames.WEST:
                        vehicleGroup.Add(new VehicleGroupData(DirOfMovement.WEST_EAST, (int)entry.Value, maxVel));
                        break;

                    case ControllerVariablesNames.NLANES:
                        roadsData = createRoadData((int)entry.Value);
                        break;

                    default:
                        break;
                }
            }

            scenario.SetVehicles(vehicleGroup);
            scenario.SetRoads(roadsData);
        }

        /**
         *
         * @param nlanes number of lanes
         * @return create all roadsData
         */
        private HashSet<RoadData> createRoadData(int nlanes)
        {
            HashSet<RoadData> roadsData = new HashSet<RoadData>();
            roadsData.Add(new RoadData("WEST_EAST", nlanes, DirOfMovement.WEST_EAST));
            roadsData.Add(new RoadData("SOUTH_NORTH", nlanes, DirOfMovement.SOUTH_NORTH));
            return roadsData;
        }

        /**
         * @return the scenario
         */
        public ScenarioData GetScenarioData()
        {
            return scenario;
        }

        /**
         * @param scenario the scenario to set
         */
        public void SetScenarioData(ScenarioData scenario)
        {
            this.scenario = scenario;
        }
    }
}
