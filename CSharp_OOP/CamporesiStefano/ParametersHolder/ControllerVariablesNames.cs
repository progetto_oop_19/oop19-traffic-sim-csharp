﻿namespace CSharp_OOP.CamporesiStefano.ParametersHolder
{
    public enum ControllerVariablesNames
    {

        /**
         *
         */
        NORTH,

        /**
         *
         */
        SOUTH,

        /**
         *
         */
        EAST,

        /**
         *
         */
        WEST,

        /**
         *
         */
        MAXVEL,

        /**
         *
         */
        NLANES,
    }
}
