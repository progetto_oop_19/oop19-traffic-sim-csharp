﻿using CSharp_OOP.BaccaRiccardo.TrafficLight;

namespace CSharp_OOP.CamporesiStefano.ParametersHolder
{
    public class VehicleGroupData
    {
        private DirOfMovement sense;
        private int number;
        private int maxVel;

        public VehicleGroupData(DirOfMovement sense,  int number, int maxVel)
        {
            this.sense = sense;
            this.number = number;
            this.maxVel = maxVel;
        }

        public DirOfMovement GetSense()
        {
            return sense;
        }

        public void SetSense(DirOfMovement sense)
        {
            this.sense = sense;
        }

        public int GetNumber()
        {
            return this.number;
        }

        public void SetNumber(int number)
        {
            this.number = number;
        }

        /**
         * @return the maxVel
         */
        public int GetMaxVel()
        {
            return maxVel;
        }

        /**
         * @param maxVel the maxVel to set
         */
        public void SetMaxVel(int maxVel)
        {
            this.maxVel = maxVel;
        }

    }
}
